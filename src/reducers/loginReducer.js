import loginState from "../store/login";

export default function loginReducer(state = loginState, action) {
  switch (action.type) {
    case "SHOW_ERROR_MESSAGE_LOG":
      return Object.assign({}, state, {
        login: {
          ...state.login,
          variant: action.payload.variant,
          validationMessage: action.payload.message,
          isMsgBoxOpen: action.payload.isOpen
        }
      });
    case "CHANGE_LOGIN_VALUE":
      return Object.assign({}, state, {
        login: {
          ...state.login,
          [action.payload.stateName]: action.payload.stateValue
        }
      });
    case "CLOSE_MESSAGE_BOX_LOG":
      return Object.assign({}, state, {
        login: {
          ...state.login,
          isMsgBoxOpen: false
        }
      });

    case "LOGIN_USER":
      localStorage.setItem("test-token", action.payload);
      return Object.assign({}, state, {
        login: {
          ...state.login,
          password: "",
          email: ""
        },
        isLogin: true
      });

    case "INIT_LOGIN_STATE":
      return Object.assign({}, state, {
        login: {
          ...state.login,
          email: action.payload.email,
          name: action.payload.name,
          mobile: action.payload.mobile,
          city: action.payload.city,
          state: action.payload.state,
          pincode: action.payload.pincode,
          profilePic:action.payload.profilePic
        },
        isLogin: true
      });
    case "LOGOUT_USER":
      localStorage.setItem("test-token", "");
      return Object.assign({}, state, {
        login: {
          ...state.login,
          email: "",
          name: "",
          password: ""
        },
        isLogin: false
      });
      case "OPEN_NAV":
          return Object.assign({},state,
            {openLeftNav:action.payload});
    default:
      return state;
  }
}
