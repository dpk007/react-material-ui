import { combineReducers } from 'redux'
import registrationReducer from './demoReducer'
import loginReducer from './loginReducer'
import productReducer from './productReducer'


export default combineReducers({
  registrationReducer,
  loginReducer,
  productReducer
})