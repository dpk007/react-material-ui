import registration from "../store/registration";

export default function registrationReducer(state = registration, action) {
  switch (action.type) {
    case "REGISTRATION_START":
      return Object.assign({}, state, {
        registration: {
          ...state.registration,
          validationMessage: "Error message"
        }
      });
    case "CLOSE_MESSAGE_BOX_REGISTER":
      return Object.assign({}, state, {
        registration: { ...state.registration, isOpen: false }
      });
    case "CHANGE_REG_VALUE":
      return Object.assign({}, state, {
        registration: {
          ...state.registration,
          [action.payload.stateName]: action.payload.stateValue
        }
      });
    case "START_LOADING":
      return Object.assign({}, state, { isLoading: "flex" });
    case "STOP_LOADING":
      return Object.assign({}, state, { isLoading: "none" });
    case "SHOW_SUCCESS_MESSAGE":
      return Object.assign({}, state, {
        registration: {
          ...state.registration,
          variant: action.payload.variant,
          validationMessage: action.payload.message,
          isOpen: action.payload.isOpen
        }
      });
    case "SHOW_ERROR_MESSAGE_REG":
      return Object.assign({}, state, {
        registration: {
          ...state.registration,
          variant: action.payload.variant,
          validationMessage: action.payload.message,
          isOpen: action.payload.isOpen
        }
      });
    case "RESET_REGISTRATION_FORM":
      return Object.assign({}, state, {
        registration: {
          ...state.registration,
          name: "",
          password: "",
          email: ""
        }
      });
    default:
      return state;
  }
}
