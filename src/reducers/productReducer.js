import product from "../store/product";

export default function loginReducer(state = product, action) {
  switch (action.type) {
    case "PRODUCTS_INFO":
      return Object.assign({}, state, { productList: action.payload });
    case "SINGAL_PRODUCT_INFO":
      return Object.assign({}, state, {
        productName: action.payload.name,
        price: action.payload.price,
        quantity: action.payload.quantity
      });
    case "UPDATE_PRODUCT_LIST":
      return Object.assign({}, state, {
        productList: [
          ...state.productList.filter(item => item._id !== action.payload)
        ]
      });
    case "PRODUCT_INPUT_CHANGE":
      return Object.assign({}, state, { [action.payload.name]:action.payload.value });
    default:
      return state;
  }
}
