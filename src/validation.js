import validator from 'validator';

function isEmpty(value){
    return validator.isEmpty(value);
}

function isEmail(value){
    return validator.isEmail(value);
}

function isLength(value){
    return validator.isLength(value,{min:5, max:15});
}

function matches(value,pattern){
    return validator.matches(value,pattern);
}

export { isEmpty ,isEmail,isLength,matches };