const loginState = {
  isLogin: false,
    login:{
      name:'',
      password:'',
      email:'',
      isMsgBoxOpen:false,
      validationMessage:'',
      variant:'error',
      state:'',
      city:'',
      pincode:'',
      mobile:null,
      profilePic:''
    },
    openLeftNav:false
    
}

export default loginState;