const registration = {
    isLoading:'none',
    registration:{
      name:'',
      password:'',
      email:'',
      validationMessage:'',
      variant:'error',
      isOpen:false
    }
}

export default registration;