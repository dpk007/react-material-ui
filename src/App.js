import React, { Component } from "react";
import { Header, Footer, Sidebar } from "./components/layout/index";
import Main from "./components/Main";
import "./css/App.css";
import Progress from "./components/Progress";
import { connect } from "react-redux";
import { withRouter } from 'react-router-dom';
import AuthHelperMethods from './components/AuthHelperMethods';
import { initLoginState,getProfiledata } from './action/index';


class App extends Component {
   
  componentDidMount(){
     const auth = new AuthHelperMethods();
     if(auth.loggedIn()){
       const {email} = auth.getTokenData();
       this.props.getProfile(email);
     } 
  }

  render() {
    let overLayStyle = {
      display:this.props.isLoading
    }
    return (
      <>
        <div className="overlay" style={overLayStyle}>
          <Progress />
        </div>
        <Sidebar />
        <div id="main" className="app">
          <Header />
          <Main />
          <Footer />
        </div>
      </>
    );
  }
}


const mapStateToProps = state => ({
  isLoading: state.registrationReducer.isLoading
});

const mapDispatchToProps = dispatch => ({
   getProfile: (email) => dispatch(getProfiledata(email))
});

export default withRouter(connect(mapStateToProps,mapDispatchToProps)(App));
