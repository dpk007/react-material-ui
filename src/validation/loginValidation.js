import validator from 'validator';


function loginValidater(email,password){
    
    if(validator.isEmpty(email)){
      return 'Email field can\'t be empty' ;
    }else if(validator.isEmpty(password)){
        return 'Password field is empty' ;
    }else{
        return true;
    }

}

export { loginValidater };