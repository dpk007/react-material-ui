import validator from 'validator';


function registerationValidater(name,email,password){
    
    if(validator.isEmpty(name)){
       return 'Name field is empty.';
    }else if(validator.isEmpty(email)){
      return 'Email field can\'t be empty' ;
    }else if(validator.isEmpty(password)){
        return 'Password field is empty' ;
    }else if(validator.isLength(name)){
        return 'Name must be greater than 5 character';
    }else if(!validator.isEmail(email)){
        return 'Email is not a valid email';
    }else if(validator.isLength(password)){
       return 'Password must be greater than 5 character';
    }else{
        return true;
    }

}

export { registerationValidater };