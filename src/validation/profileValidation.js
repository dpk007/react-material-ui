import validator from "validator";

 function profileValidater(value) {
  if (validator.isEmpty(value.email)) {
    return "Email field is Empty";
  } else if (validator.isEmpty(value.city)) {
    return "City field is Empty";
  } else if (validator.isEmpty(value.state)) {
    return "State field is Empty";
  } else if (validator.isEmpty(value.mobile.toString())) {
    return "Mobile No. field is Empty";
  } else if (validator.isEmpty(value.pincode.toString())) {
    return "Pincode field is Empty";
  } else {
    return true;
  }
}

export default profileValidater;