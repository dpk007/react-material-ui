import axios from "axios";
import { registerationValidater } from "../validation/registerValidation";
import { loginValidater } from "../validation/loginValidation";
import profileValidater from "../validation/profileValidation";

export const changeLoginValue = data => ({
  type: "CHANGE_LOGIN_VALUE",
  payload: data
});

export const changeRegValue = data => ({
  type: "CHANGE_REG_VALUE",
  payload: data
});

export const startRegistration = () => ({
  type: "REGISTRATION_START"
});

// export const registerUsers = () => ({
//   type: "REGISTRATION_START"
// })

export const closeMessageBoxReg = () => ({
  type: "CLOSE_MESSAGE_BOX_REGISTER"
});

export const startLoading = () => ({
  type: "START_LOADING"
});

export const stopLoading = () => ({
  type: "STOP_LOADING"
});

export const showSuccessMessage = data => ({
  type: "SHOW_SUCCESS_MESSAGE",
  payload: data
});

export const openNavigation = (value) => ({
  type:'OPEN_NAV',
  payload:value
})

export const showErrorMessageRegister = data => ({
  type: "SHOW_ERROR_MESSAGE_REG",
  payload: data
});

export const showErrorMessageLogin = data => ({
  type: "SHOW_ERROR_MESSAGE_LOG",
  payload: data
});

export const closeMessageBoxLog = () => ({
  type: "CLOSE_MESSAGE_BOX_LOG"
});

export const resetRegistrationForm = () => ({
  type: "RESET_REGISTRATION_FORM"
});

export const loginUserInApp = data => ({
  type: "LOGIN_USER",
  payload: data
});

export const initLoginState = data => ({
  type: "INIT_LOGIN_STATE",
  payload: data
});

export const productInfo = data => ({
  type : 'PRODUCTS_INFO',
  payload:data
})

export const logoutUser = history => (dispatch, getState) => {
  dispatch({
    type: "LOGOUT_USER"
  });
  history.push("/login");
};

export const singalProductInfo = data =>({
   type:'SINGAL_PRODUCT_INFO',
   payload:data
})

export const updateProductList = data => ({
  type:'UPDATE_PRODUCT_LIST',
  payload:data
})

export function login(data, history) {
  return function(dispatch) {
    dispatch(startLoading());

    let isValid = false;
    let loginValidationMsg = loginValidater(data.email, data.password);
    if (loginValidationMsg === true) {
      isValid = true;
    }

    if (isValid) {
      return axios({
        method: "post",
        url: "http://localhost:9000/login",
        data: {
          email: data.email,
          password: data.password
        },
        headers: {
          Accept: "application/json, text/plain, */*",
          "Content-Type": "application/json"
        }
      })
        .then(response => {
          dispatch(stopLoading());
          dispatch(loginUserInApp(response.data.token));
          dispatch(initLoginState(response.data.info));
          history.push("/dashboard");
        })
        .catch(error => {
          dispatch(stopLoading());
          dispatch(
            showErrorMessageLogin({
              variant: "error",
              message:
                error.response && typeof error.response.data.errMsg != undefined
                  ? error.response.data.errMsg
                  : error.message,
              isOpen: true
            })
          );
        });
    } else {
      dispatch(stopLoading());
      dispatch(
        showErrorMessageLogin({
          variant: "error",
          message: loginValidationMsg,
          isOpen: true
        })
      );
    }
  };
}

export function registerUsers(data) {
  // Thunk middleware knows how to handle functions.
  // It passes the dispatch method as an argument to the function,
  // thus making it able to dispatch actions itself.

  return function(dispatch) {
    // First dispatch: the app state is updated to inform
    // that the API call is starting.

    dispatch(startLoading());

    // if (isEmpty(this.state.name)) {
    //   this.setState({
    //     isOpen: true,
    //     variant: "error",
    //     message: "User name is Empty"
    //   });
    // } else if (isEmpty(this.state.email)) {
    //   this.setState({
    //     isOpen: true,
    //     variant: "error",
    //     message: "Email field is Empty"
    //   });
    // } else if (!isEmail(this.state.email)) {
    //   this.setState({
    //     isOpen: true,
    //     variant: "error",
    //     message: "Email formate is not valid"
    //   });
    // } else if (isEmpty(this.state.password)) {
    //   this.setState({
    //     isOpen: true,
    //     variant: "error",
    //     message: "Password field can not be empty"
    //   });
    // } else if (!isLength(this.state.password)) {
    //   this.setState({
    //     isOpen: true,
    //     variant: "error",
    //     message: "Password length should be in range of 5 - 15"
    //   });
    // } else {
    // The function called by the thunk middleware can return a value,
    // that is passed on as the return value of the dispatch method.

    // In this case, we return a promise to wait for.
    // This is not required by thunk middleware, but it is convenient for us.
    let isValid = false;
    const validationMessage = registerationValidater(
      data.name,
      data.email,
      data.password
    );
    if (validationMessage === true) {
      isValid = true;
    }

    if (isValid) {
      return axios({
        method: "post",
        url: "http://localhost:9000/register",
        data: {
          Name: data.name,
          Email: data.email,
          Password: data.password
        }
      })
        .then(response => {
          dispatch(stopLoading());
          dispatch(
            showSuccessMessage({
              variant: "success",
              message: response.data.message,
              isOpen: true
            })
          );

          dispatch(resetRegistrationForm());
        })
        .catch(error => {
          dispatch(stopLoading());
          dispatch(
            showErrorMessageRegister({
              variant: "error",
              message:
                typeof error.response.data.errMsg != undefined
                  ? error.response.data.errMsg
                  : error.message,
              isOpen: true
            })
          );
        });
      // }
    } else {
      dispatch(stopLoading());
      dispatch(
        showErrorMessageRegister({
          variant: "error",
          message: validationMessage,
          isOpen: true
        })
      );
    }
  };
}

/*
 profile function

*/

export function saveProfileInformation(data) {
  return function(dispatch) {
    dispatch(startLoading());

    let isValid = false;
    const validationMessage = profileValidater({
      name: data.name,
      email: data.email,
      city: data.city,
      state: data.state,
      pincode: data.pincode,
      mobile: data.mobile
    });
    if (validationMessage === true) {
      isValid = true;
    }

    if (isValid) {
      let token = localStorage.getItem("test-token");
      return axios({
        method: "post",
        url: "http://localhost:9000/profile",
        data: {
          name: data.name,
          email: data.email,
          state: data.state,
          city: data.city,
          pincode: data.pincode,
          mobile: data.mobile
        },
        headers: {
          Authorization: token
        }
      })
        .then(response => {
          dispatch(stopLoading());
          dispatch(
            showSuccessMessage({
              variant: "success",
              message: response.data.message,
              isOpen: true
            })
          );
          // dispatch(initLoginState(response.data));
        })
        .catch(err => {
          dispatch(stopLoading());
          dispatch(
            showSuccessMessage({
              variant: "error",
              message:
                typeof err.response.data.errMsg != undefined
                  ? err.response.data.errMsg
                  : err.message,
              isOpen: true
            })
          );
        });
    } else {
      dispatch(stopLoading());
      dispatch(
        showSuccessMessage({
          variant: "error",
          message: validationMessage,
          isOpen: true
        })
      );
    }
  };
}

export function getProfiledata(email) {
  return function(dispatch) {
    let token = localStorage.getItem("test-token");
    return axios({
      method: "GET",
      url: "http://localhost:9000/profile?email=" + email,
      headers: {
        Authorization: token
      }
    })
      .then(response => {
        dispatch(initLoginState(response.data));
      })
      .catch(err => {});
  };
}

/*
  product function
*/

export const productInputChange = (data) =>({
   type:'PRODUCT_INPUT_CHANGE',
   payload:data
})

export function addProductInfo(data) {
  return function(dispatch) {
    let token = localStorage.getItem("test-token");
    return axios({
      method: "POST",
      url: "http://localhost:9000/product",
      data: data,
      headers: {
        Authorization: token
      }
    })
      .then(response => {
        dispatch(
          showSuccessMessage({
            variant: "success",
            message: response.data.message,
            isOpen: true
          })
        );
      })
      .catch(err => {
        dispatch(
          showSuccessMessage({
            variant: "error",
            message:
              typeof err.response.data.errMsg != undefined
                ? err.response.data.errMsg
                : err.message,
            isOpen: true
          })
        );
      });
  };
}

export function editProductInfo(data) {
  return function (dispatch){
    let token = localStorage.getItem("test-token");
    return axios({
      method: "POST",
      url: "http://localhost:9000/product/editProduct",
      data:data,
      headers: {
        Authorization: token
      }
    })
      .then(response => {
        dispatch(
          showSuccessMessage({
            variant: "success",
            message: "Successfuly updated.",
            isOpen: true
          })
        );
      })
      .catch(err => {
        dispatch(
          showSuccessMessage({
            variant: "error",
            message: "Not able to update.",
            isOpen: true
          })
        );
      });
  }
}

export function getProductData() {
  return function(dispatch) {
    let token = localStorage.getItem("test-token");
    return axios({
      method: "GET",
      url: "http://localhost:9000/product",
      headers: {
        Authorization: token
      }
    })
      .then(response => {
          dispatch(productInfo(response.data));
      })
      .catch(err => {
            
      });
  };
}

/*
GET singal product data
*/
export function getSingalProductData(data) {
  return function(dispatch) {
    let token = localStorage.getItem("test-token");
    return axios({
      method: "POST",
      url: "http://localhost:9000/product/productDetails",
      data:data,
      headers: {
        Authorization: token
      }
    })
      .then(response => {
          dispatch(singalProductInfo(response.data));
      })
      .catch(err => {
            
      });
  };
}

/*
Delete product information
*/

export function deleteProduct(id){
  return function(dispatch) {
    let token = localStorage.getItem("test-token");
    return axios({
      method: "POST",
      url: "http://localhost:9000/product/productDelete",
      data:{id:id},
      headers: {
        Authorization: token
      }
    })
      .then(response => {
        dispatch(updateProductList(id));
        dispatch(
          showSuccessMessage({
            variant: "success",
            message: 'Successfully deleted',
            isOpen: true
          })
        );
      })
      .catch(err => {
        dispatch(
          showSuccessMessage({
            variant: "error",
            message: 'Product not able to delete.',
            isOpen: true
          })
        );
      });
  };
}
