import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Input from './Input';
import Message from './Message';
import { connect } from 'react-redux';
import { saveProfileInformation,closeMessageBoxReg } from '../action/index';

const styles = {
  card: {
    width:550,
    margin:30,
    marginLeft:'30%',
    textAlign:'center'
  },
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  title: {
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
  placeCenter:{
    height: 'auto',
    margin: '0 auto',
    padding: '10',
    position: 'relative',
  }
};

class Profile extends Component {
 
    constructor(props){
        super(props);    
       this.handleInputChange = this.handleInputChange.bind(this);
       this.state = {'name':this.props.name,'email':this.props.email,'city':this.props.city,'state':this.props.state,'pincode':this.props.pincode,'mobile':this.props.mobile};
      }

    handleInputChange(stateName,stateValue){
         this.setState({[stateName]:stateValue});
    }

render(){
  return (
      <>
     <Message variant={this.props.variant} message={this.props.message} onMessageClose={this.props.closeMessageBox} open={this.props.isOpen} />
    <Card className={this.props.classes.card}>
      <CardContent>
        <Typography variant="h6" color="textSecondary" gutterBottom>
         Edit User Information
        </Typography>
    
        
         <Typography variant="div" className={this.props.classes.container}>
         
           <Input id="user-name" type="test" label="Name" name="name" value={this.state.name} onInputChange={this.handleInputChange} />
           <Input id="user-email" type="email" label="Email" name="email" value={this.state.email} onInputChange={this.handleInputChange} />
         
           <Input id="user-city" type="text" label="city" name="city" value={this.state.city} onInputChange={this.handleInputChange} />
           <Input id="user-state" type="state" label="state" name="state" value={this.state.state} onInputChange={this.handleInputChange} />
           <Input id="user-pincode" type="text" label="pincode" name="pincode" value={this.state.pincode} onInputChange={this.handleInputChange} />
          
           <Input id="user-number" type="numeric" label="Mobile" name="mobile" value={this.state.mobile === null ? '' :this.state.mobile } onInputChange={this.handleInputChange} />
          
          </Typography>
      </CardContent>
      <CardActions>
        <Button className={this.props.classes.placeCenter} variant="outlined" color="primary" size="medium" onClick={() =>this.props.saveUser(this.state)}>Save</Button>
      </CardActions>
    </Card>
    </>
    )
  };
  
}

const mapStateToProps = (state) => {
  return {
    name:state.loginReducer.login.name,
    email:state.loginReducer.login.email,
    state:state.loginReducer.login.state,
    city:state.loginReducer.login.city,
    pincode:state.loginReducer.login.pincode,
    mobile:state.loginReducer.login.mobile,
    variant:state.registrationReducer.registration.variant,
    isOpen :state.registrationReducer.registration.isOpen,
    message: state.registrationReducer.registration.validationMessage,
  }
};

const mapDispatchToProps = (dispatch) => ({
   saveUser:(data) =>  dispatch(saveProfileInformation(data)),
   closeMessageBox: () => dispatch(closeMessageBoxReg())
});

Profile.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default connect(mapStateToProps,mapDispatchToProps)(withStyles(styles)(Profile));