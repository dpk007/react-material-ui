import React, { Component } from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import Input from "./Input";
import Message from "./Message";
import { connect } from "react-redux";
import { registerUsers, closeMessageBoxReg, changeRegValue } from "../action";
import AuthHelperMethods from './AuthHelperMethods';

const styles = {
  card: {
    width: 300,
    margin: 30,
    marginLeft: "40%",
    textAlign: "center"
  },
  title: {
    fontSize: 14
  },
  pos: {
    marginBottom: 12
  }
};

class Register extends Component {
  constructor(props) {
    super(props);
    
  }

  componentWillMount(){
    const auth = new AuthHelperMethods();
    if( auth.loggedIn()){
       this.props.history.push('/dashboard');
    }
  }

  render() {


    return (
      <>
        <Message
          variant={this.props.variant}
          message={this.props.message}
          onMessageClose={this.props.closeMessageBox}
          open={this.props.isOpen}
        />
        <Card className={this.props.classes.card}>
          <CardContent>
            <Typography variant="h3" color="textSecondary" gutterBottom>
              Register
            </Typography>

            <Typography component="div">
              <Input
                id="user-name"
                value={this.props.name}
                type="text"
                label="Name"
                name="name"
                onInputChange={this.props.handleInputChange}
              />

              <Input
                id="user-email"
                value={this.props.email}
                type="email"
                label="Email"
                name="email"
                onInputChange={this.props.handleInputChange}
              />

              <Input
                id="user-pass"
                value={this.props.password}
                type="password"
                label="Password"
                name="password"
                onInputChange={this.props.handleInputChange}
              />
            </Typography>
          </CardContent>
          <CardActions>
            <Button
              variant="outlined"
              color="primary"
              size="medium"
              onClick={()=>this.props.registerUser({name:this.props.name,email:this.props.email,password:this.props.password})}
            >
              Register
            </Button>
          </CardActions>
        </Card>
      </>
    );
  }
}

Register.propTypes = {
  classes: PropTypes.object.isRequired
};

const mapStateToProps = state => {
    
  return{
  variant: state.registrationReducer.registration.variant,
  message: state.registrationReducer.registration.validationMessage,
  name: state.registrationReducer.registration.name,
  password: state.registrationReducer.registration.password,
  email: state.registrationReducer.registration.email,
  isOpen: state.registrationReducer.registration.isOpen
 }

};

const mapDispatchToProps = (dispatch) => {
  
  return {
    registerUser: (data) => dispatch(registerUsers(data)),
    closeMessageBox: () => dispatch(closeMessageBoxReg()),
    handleInputChange: (stateName, stateValue) =>
      dispatch(changeRegValue({ stateName: stateName, stateValue: stateValue }))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withStyles(styles)(Register));
