import React, { Component } from 'react';
import {Redirect,Route} from 'react-router-dom';
import AuthHelperMethods from './AuthHelperMethods';

const auth = new AuthHelperMethods();
const PrivateRoute = ({ component: Component, ...rest }) => (

    <Route {...rest} render={(props) => (
      auth.loggedIn()
        ? <Component {...props} />
        : <Redirect to='/login' />
    )} />
  )
 
export default PrivateRoute;