import React, { Component } from "react";
import EnhancedTable from "./table";
import { connect } from "react-redux";
import { getProductData, closeMessageBoxReg ,deleteProduct} from "../action";
import Message from "./Message";
import ResponsiveDialog from "./Dialog";

class Dashboard extends Component {
  constructor(props) {
    super(props);
    this.state = { deleteId: "", isOpen: false };
    this.handleProductDel = this.handleProductDel.bind(this);
    this.handleConfirmMessage = this.handleConfirmMessage.bind(this);
  }

  handleConfirmMessage(message) {
    this.setState({ isOpen: false });
    if (!message) {
      this.setState({ deleteId: "", isOpen: false });
      return;
    }
    this.props.deleteProduct(this.state.deleteId);
  }

  handleProductDel(deleteIds) {
      this.setState({ deleteId: deleteIds, isOpen: true });
  }

  componentWillMount() {
    //get product
    this.props.getProduct();
  }

  render() {
    const headData = [
      { id: "name", numeric: false, disablePadding: true, label: "Name" },
      {
        id: "quantity",
        numeric: true,
        disablePadding: false,
        label: "Quantity"
      },
      {
        id: "price",
        numeric: true,
        disablePadding: false,
        label: "Price (per)"
      },
      { id: "action", numeric: false, disablePadding: false, label: "Action" }
    ];

    return (
      <>
        <Message
          variant={this.props.variant}
          message={this.props.message}
          onMessageClose={this.props.closeMessageBox}
          open={this.props.isOpen}
        />
        <ResponsiveDialog
          title="Delete product confarmation"
          content="You realy want to delete this product."
          cancel="No"
          agree="Yes"
          onDelConfirm={this.handleConfirmMessage}
          isOpen={this.state.isOpen}
        />
        <EnhancedTable
          heading="Product"
          rows={headData}
          onProductDel={this.handleProductDel}
          data={this.props.product}
        />
      </>
    );
  }
}

const mapStateToProps = state => {
  return {
    variant: state.registrationReducer.registration.variant,
    message: state.registrationReducer.registration.validationMessage,
    isOpen: state.registrationReducer.registration.isOpen,
    product: state.productReducer.productList
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getProduct: () => dispatch(getProductData()),
    closeMessageBox: () => dispatch(closeMessageBoxReg()),
    deleteProduct: (id) => dispatch(deleteProduct(id)),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Dashboard);
