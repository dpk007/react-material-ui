import React from "react";
import PropTypes from "prop-types";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import withMobileDialog from "@material-ui/core/withMobileDialog";

class ResponsiveDialog extends React.Component {
  constructor(props) {
    super(props)
  }

  handleClose = (message) => {
    this.props.onDelConfirm(message);
    this.setState({ });
  };

  render() {
    const { fullScreen } = this.props;

    return (
      <div>
        
        <Dialog
          fullScreen={fullScreen}
          open={this.props.isOpen}
          onClose={this.handleClose}
          aria-labelledby="responsive-dialog-title"
        >
          <DialogTitle id="responsive-dialog-title">
            {this.props.title}
          </DialogTitle>
          <DialogContent>
            <DialogContentText>
              {this.props.content}
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={() => this.handleClose(false)} color="primary">
              {this.props.cancel}
            </Button>
            <Button onClick={()=>this.handleClose(true)} color="primary" autoFocus>
              {this.props.agree}
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}

ResponsiveDialog.propTypes = {
  fullScreen: PropTypes.bool.isRequired
};

export default withMobileDialog()(ResponsiveDialog);
