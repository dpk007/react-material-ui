import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Cards from './Cards';
import '../css/home.css';

const styles = {
  root: {
    flexGrow: 1,
  },
  paper: {
    height: 140,
    width: 100,
  },
};

class Home extends Component {
    
 
render(){
  return (
  
      <div className="column2">
          <Grid container className={this.props.classes.root} >
               <Grid item xs={12}>
                  <Grid container className={this.props.classes.demo} justify="center">
          
                       <Cards/>
    
             </Grid>
          </Grid>
       </Grid>

     </div>
    
  );
    }

    
}

Home.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Home);