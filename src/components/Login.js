import React, { Component } from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import Input from "./Input";
import Message from "./Message";
import { changeLoginValue ,login,closeMessageBoxLog } from "../action";
import { connect } from "react-redux";
import { withRouter } from 'react-router-dom';
import AuthHelperMethods from './AuthHelperMethods';


const styles = {
  card: {
    width: 300,
    margin: 30,
    marginLeft: "40%",
    textAlign: "center"
  },
  title: {
    fontSize: 14
  },
  pos: {
    marginBottom: 12
  }
};

class Login extends Component {
  constructor(props) {
    super(props);
  }

  componentWillMount(){
    const auth = new AuthHelperMethods();
    if( auth.loggedIn()){
       this.props.history.push('/dashboard');
    }
  }


  render() {
    return (
      <>
        <Message
          variant={this.props.variant}
          message={this.props.message}
          onMessageClose={this.props.closeMessageBox}
          open={this.props.isOpen}
        />
        <Card className={this.props.classes.card}>
          <CardContent>
            <Typography variant="h3" color="textSecondary" gutterBottom>
              Login
            </Typography>

            <Typography component="div">
              <Input
                id="user-email"
                type="email"
                label="Email"
                name="email"
                value={this.props.email}
                onInputChange={this.props.handleInputChange}
              />

              <Input
                id="user-pass"
                type="password"
                label="Password"
                value={this.props.password}
                name="password"
                onInputChange={this.props.handleInputChange}
              />
            </Typography>
          </CardContent>
          <CardActions>
            <Button
              variant="outlined"
              color="primary"
              size="medium"
              onClick={()=>this.props.loginUser({email:this.props.email,password:this.props.password},this.props.history)}
            >
              Login
            </Button>
          </CardActions>
        </Card>
      </>
    );
  }
}

Login.propTypes = {
  classes: PropTypes.object.isRequired
};

const mapStateToProps = state => {
 
  return{
    variant: state.loginReducer.login.variant,
  message: state.loginReducer.login.validationMessage,
  password: state.loginReducer.login.password,
  email: state.loginReducer.login.email,
  isOpen: state.loginReducer.login.isMsgBoxOpen,
  isSigin: state.loginReducer.isLogin
  }
};

const mapDispatchToProps = dispatch => ({
  closeMessageBox: ()=> dispatch(closeMessageBoxLog()),
  handleInputChange: (stateName, stateValue) =>
    dispatch(changeLoginValue({ stateName: stateName, stateValue: stateValue })),
    loginUser : (data,history) => dispatch(login(data,history))
});

export default withRouter(connect(
  mapStateToProps,
  mapDispatchToProps
)(withStyles(styles)(Login)));
