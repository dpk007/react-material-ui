import React, { Component } from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import CircularProgress from "@material-ui/core/CircularProgress";


const styles = theme => ({
 
});

class Progress extends Component {

    constructor(props){
          super(props);
        
    }

  render() {
    return (
      <>
        <CircularProgress />
     </>
    );
  }
}

Progress.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(Progress);
