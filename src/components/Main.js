import React, { Component } from 'react';
import Home from './Home';
import Post from './Post';
import PostWrite from './PostWrite';
import Register from './Register';
import Login from './Login';
import Dashboard from './Dashboard'
import AddProduct from './AddProduct';
import Profile from './Profile';
import EditProduct from './EditProduct';
import PrivateRoute from './PrivateRoute';
import { Switch, Route } from 'react-router-dom';

class Main extends Component {
render(){
    return (
      
      <Switch>
        <Route exact path='/' component={Home}/>
        <Route  path='/post/:id' component={Post}/>
        <Route  path='/write' component={PostWrite}/>
        <Route  path='/register' component={Register}/>
        <Route  path='/login' component={Login}/>
        <PrivateRoute  path='/add-products' component={AddProduct}/>
        <PrivateRoute  path='/profile' component={Profile}/>
        <PrivateRoute  path='/dashboard' component={Dashboard}/>
        <PrivateRoute  path='/editProduct/:id' component={EditProduct}/>
      </Switch>
    
   )
  }
}

export default Main;
