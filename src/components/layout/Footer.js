import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import BottomNavigation from '@material-ui/core/BottomNavigation';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';


const styles = {
  root: {
    flexGrow: 1,
  },

};

class Footer extends Component {

    constructor(props){
       super(props);
       
    }

render(){
    return (
        <BottomNavigation
          className={this.props.root}
        >

           <Toolbar>
                <Typography variant="title" >
                copyright@ 2019
                </Typography>
            </Toolbar>
        </BottomNavigation>
      );
   }
}

Footer.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Footer);