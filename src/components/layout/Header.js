import React, { Component } from "react";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";
import Button from "@material-ui/core/Button";
import { withStyles } from "@material-ui/core/styles";
import { Link } from "react-router-dom";
import Avatar from "@material-ui/core/Avatar";
import { logoutUser,openNavigation  } from "../../action";
import { connect } from "react-redux";
import {withRouter} from 'react-router-dom';

const styles = {
  root: {
    flexGrow: 1
  },
  grow: {
    flexGrow: 1
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20
  },
  rightToolbar: {
    marginLeft: "auto",
    marginRight: 0
  },
  link: {
    textDecoration: "none",
    color: "#ffffff"
  }
};

class Header extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  

  render() {
    const isSignup = this.props.isSignup;
    let button;
    if (!isSignup) {
      button = 
        [<Button id="loginButton" color="inherit">
          <Link className={this.props.classes.link} to="/login">
            Login
          </Link>
     </Button>,

        <Button id="registerButton" color="inherit">
          <Link className={this.props.classes.link} to="/register">
            Register
          </Link>
        </Button>]
    
    } else {
      button = 
        <Button id="logoutButton" color="inherit" onClick={()=>{this.props.logout(this.props.history)}}>    
            Logout
        </Button>
      
    }
    return (
      <div className={this.props.root}>
        <AppBar position="static">
          <Toolbar>
            <IconButton
              className={this.props.classes.menuButton}
              color="inherit"
              aria-label="Menu"
              onClick={() => this.props.openNav(true)}
            >
              <MenuIcon />
            </IconButton>
            <Typography
              variant="h6"
              color="inherit"
              className={this.props.classes.grow}
            >
              News
            </Typography>
            {/* <div onClick={this.props.toggalButton}>Toggal</div> */}
            <div className={this.props.classes.rightToolbar}>{button}</div>
            {this.props.isSignup ? (
              <Avatar
                alt="Remy Sharp"
                margin="30"
                src={'http://localhost:9000/images/'+this.props.profilePic}
              />
            ) : (
              ""
            )}
          </Toolbar>
        </AppBar>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    isSignup: state.loginReducer.isLogin,
    profilePic:state.loginReducer.login.profilePic
  };
};

const mapDispatchToProps = dispatch => ({
  logout : (history) => dispatch(logoutUser(history)),
  openNav: (value) => dispatch(openNavigation(value))
});

export default withRouter(connect(
  mapStateToProps,
  mapDispatchToProps
)(withStyles(styles)(Header)));
