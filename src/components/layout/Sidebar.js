import React, { Component } from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Drawer from "@material-ui/core/Drawer";
import List from "@material-ui/core/List";
import Divider from "@material-ui/core/Divider";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import { connect } from "react-redux";
import { openNavigation } from "../../action";
import { Link } from "react-router-dom";

const styles = {
  list: {
    width: 250
  },
  fullList: {
    width: "auto"
  },
  link: {
    textDecoration: "none",
    color: "#ffffff"
  }
};

class Sidebar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      top: false,
      left: false,
      bottom: false,
      right: false
    };
    this.toggleDrawer = this.toggleDrawer.bind();
  }

  toggleDrawer = (side, open) => () => {
    this.setState({
      [side]: open
    });
  };

  render() {
    const { classes } = this.props;
     const loginList = this.props.isSignup
      ? [
          <List key="afterLoginNav">
            {[
              { name: "Profile", href: "/profile" },
              { name: "Add Product", href: "/add-products" },
              { name: "Dashboard", href: "/dashboard" }
            ].map((text, index) => (
              <Link className={classes.link} to={text.href}>
                <ListItem button key={text.name}>
                  <ListItemText primary={text.name} />
                </ListItem>
              </Link>
            ))}
          </List>
        ,<Divider key="afterLoginDiv"/>]
      : "" ;

    const sideList = (
      <div className={classes.list}>
      
        {loginList}
        <List>
          {["Home", "About Us"].map((text, index) => (
            <ListItem button key={text}>
              <ListItemText primary={text} />
            </ListItem>
          ))}
        </List>
      </div>
    );

    return (
      <div>
        <Drawer
          open={this.props.isNavOpen}
          onClick={() => this.props.openNav(false)}
        >
          <div
            tabIndex={0}
            role="button"
            onClick={() => this.props.openNav(false)}
            onKeyDown={() => this.props.openNav(false)}
          >
            {sideList}
          </div>
        </Drawer>
      </div>
    );
  }
}

Sidebar.propTypes = {
  classes: PropTypes.object.isRequired
};

const mapStateToProps = state => {
  return {
    isSignup: state.loginReducer.isLogin,
    isNavOpen: state.loginReducer.openLeftNav
  };
};

const mapDispatchToProps = dispatch => ({
  openNav: value => dispatch(openNavigation(value))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withStyles(styles)(Sidebar));
