import React, { Component } from 'react';
import { withStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import Input from "./Input";
import Message from "./Message";
import RangeSlider from './RangeSlider';
import {connect} from 'react-redux';
import { closeMessageBoxReg,addProductInfo } from '../action/index';


const styles = {
    card: {
      width: 300,
      margin: 30,
      marginLeft: "40%",
      textAlign: "center"
    },
    title: {
      fontSize: 14
    },
    pos: {
      marginBottom: 12
    }
  };
  
  class AddProduct extends Component {
    constructor(props) {
      super(props);
      this.state = {'quantity':1,'product':'','price':''}
      this.handleRangeChange = this.handleRangeChange.bind(this);
      this.handleInputChange = this.handleInputChange.bind(this);
    }

    handleRangeChange(value){
       this.setState({'quantity':value});
    }
  
    handleInputChange(name,value){
     this.setState({[name]:value});
    }
  
    render() {

      return (
        <>
          <Message
            variant={this.props.variant}
            message={this.props.message}
            onMessageClose={this.props.closeMessageBox}
            open={this.props.isOpen}
          />
          <Card className={this.props.classes.card}>
            <CardContent>
              <Typography variant="h6" color="textSecondary" gutterBottom>
                Add Product
              </Typography>
  
              <Typography component="div">
                <Input
                  id="product-name"
                  type="text"
                  label="Name"
                  name="product"
                  value={this.state.product}
                  onInputChange={this.handleInputChange}
                />
  
                <Input
                  id="product-price"
                  type="number"
                  label="Price"
                  value={this.state.price}
                  name="price"
                  onInputChange={this.handleInputChange}
                />
                 Total Quantity : {this.state.quantity}
                <RangeSlider min={1} max={10} step={1} value={this.state.quantity} onRangeChange={this.handleRangeChange}/>
              </Typography>
            </CardContent>
            <CardActions>
              <Button
                variant="outlined"
                color="primary"
                size="medium"
                onClick={()=>this.props.addProduct(this.state)}
              >
                Add
              </Button>
            </CardActions>
          </Card>
        </>
      );
    }
}

const mapStateToProps = (state)  =>({
    variant:state.registrationReducer.registration.variant,
    message:state.registrationReducer.registration.validationMessage,
    isOpen:state.registrationReducer.registration.isOpen
})

const mapDispatchToProps = (dispatch) => ({
    closeMessageBox : () => dispatch(closeMessageBoxReg()),
    addProduct: (data) => dispatch(addProductInfo(data))
})
 
export default connect(mapStateToProps,mapDispatchToProps)(withStyles(styles)(AddProduct));