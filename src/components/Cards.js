import React, { Component } from 'react';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';
import {Link} from 'react-router-dom';

const styles = {
    card: {
        maxWidth: 345,
        margin:20,
      },
      media: {
        height: 140,
      },
      link:{
          textDecoration: 'none',
      },
}


class Cards extends Component {

    constructor(props) {
        super(props);
        this.state = {  }
    }

    render() { 
        return (  
<>
    <Card className={this.props.classes.card}>
    <Link to="/post/1" className={this.props.classes.link}>
      <CardActionArea>
        <CardMedia
          className={this.props.classes.media}
          image="https://images.fineartamerica.com/images-medium-large-5/orange-juce-in-glass-with-blue-sky-raimond-klavins.jpg"
          title="Contemplative Reptile"
        />
        <CardContent>
          <Typography gutterBottom variant="h5" component="h2">
            Lizard
          </Typography>
          <Typography component="p">
            Lizards are a widespread group of squamate reptiles, with over 6,000 species, ranging
            across all continents except Antarctica
          </Typography>
        </CardContent>
      </CardActionArea>
      </Link>
      <CardActions>
        <Button size="small" color="primary">
          Share
        </Button>
        <Button size="small" color="primary">
          Learn More
        </Button>
      </CardActions>
    </Card>


<Card className={this.props.classes.card}>
<CardActionArea>
  <CardMedia
    className={this.props.classes.media}
    image="https://images.fineartamerica.com/images-medium-large-5/orange-juce-in-glass-with-blue-sky-raimond-klavins.jpg"
    title="Contemplative Reptile"
  />
  <CardContent>
    <Typography gutterBottom variant="h5" component="h2">
      Lizard
    </Typography>
    <Typography component="p">
      Lizards are a widespread group of squamate reptiles, with over 6,000 species, ranging
      across all continents except Antarctica
    </Typography>
  </CardContent>
</CardActionArea>
<CardActions>
  <Button size="small" color="primary">
    Share
  </Button>
  <Button size="small" color="primary">
    Learn More
  </Button>
</CardActions>
</Card>


<Card className={this.props.classes.card}>
<CardActionArea>
  <CardMedia
    className={this.props.classes.media}
    image="https://images.fineartamerica.com/images-medium-large-5/orange-juce-in-glass-with-blue-sky-raimond-klavins.jpg"
    title="Contemplative Reptile"
  />
  <CardContent>
    <Typography gutterBottom variant="h5" component="h2">
      Lizard
    </Typography>
    <Typography component="p">
      Lizards are a widespread group of squamate reptiles, with over 6,000 species, ranging
      across all continents except Antarctica
    </Typography>
  </CardContent>
</CardActionArea>
<CardActions>
  <Button size="small" color="primary">
    Share
  </Button>
  <Button size="small" color="primary">
    Learn More
  </Button>
</CardActions>
</Card>


<Card className={this.props.classes.card}>
<CardActionArea>
  <CardMedia
    className={this.props.classes.media}
    image="https://images.fineartamerica.com/images-medium-large-5/orange-juce-in-glass-with-blue-sky-raimond-klavins.jpg"
    title="Contemplative Reptile"
  />
  <CardContent>
    <Typography gutterBottom variant="h5" component="h2">
      Lizard
    </Typography>
    <Typography component="p">
      Lizards are a widespread group of squamate reptiles, with over 6,000 species, ranging
      across all continents except Antarctica
    </Typography>
  </CardContent>
</CardActionArea>
<CardActions>
  <Button size="small" color="primary">
    Share
  </Button>
  <Button size="small" color="primary">
    Learn More
  </Button>
</CardActions>
</Card>


<Card className={this.props.classes.card}>
<CardActionArea>
  <CardMedia
    className={this.props.classes.media}
    image="https://images.fineartamerica.com/images-medium-large-5/orange-juce-in-glass-with-blue-sky-raimond-klavins.jpg"
    title="Contemplative Reptile"
  />
  <CardContent>
    <Typography gutterBottom variant="h5" component="h2">
      Lizard
    </Typography>
    <Typography component="p">
      Lizards are a widespread group of squamate reptiles, with over 6,000 species, ranging
      across all continents except Antarctica
    </Typography>
  </CardContent>
</CardActionArea>
<CardActions>
  <Button size="small" color="primary">
    Share
  </Button>
  <Button size="small" color="primary">
    Learn More
  </Button>
</CardActions>
</Card>
</>
        );
    }
}
 
export default withStyles(styles)(Cards);