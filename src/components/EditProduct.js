import React, { Component } from 'react';
import { withStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import Input from "./Input";
import Message from "./Message";
import RangeSlider from './RangeSlider';
import {connect} from 'react-redux';
import { closeMessageBoxReg,editProductInfo,getSingalProductData,productInputChange } from '../action/index';


const styles = {
    card: {
      width: 300,
      margin: 30,
      marginLeft: "40%",
      textAlign: "center"
    },
    title: {
      fontSize: 14
    },
    pos: {
      marginBottom: 12
    }
  };
  
  class EditProduct extends Component {
    constructor(props) {
      super(props);
      this.state = {'quantity':1,'product':'','price':''}
      this.handleRangeChange = this.handleRangeChange.bind(this);
      this.handleInputChange = this.handleInputChange.bind(this);
    }

    componentWillMount(){
       let id = this.props.match.params.id; 
       this.props.getProductDetail({'id':id});               
    }

    handleRangeChange(value){
       this.props.productInputChange('quantity',value);
    }
  
    handleInputChange(name,value){
         this.props.productInputChange(name,value);
    }
  
    render() {

      return (
        <>
          <Message
            variant={this.props.variant}
            message={this.props.message}
            onMessageClose={this.props.closeMessageBox}
            open={this.props.isOpen}
          />
          <Card className={this.props.classes.card}>
            <CardContent>
              <Typography variant="h6" color="textSecondary" gutterBottom>
                Edit Product
              </Typography>
  
              <Typography component="div">
                <Input
                  id="product-name"
                  type="text"
                  label="Name"
                  name="productName"
                  value={this.props.productName}
                  onInputChange={this.handleInputChange}
                />
  
                <Input
                  id="product-price"
                  type="number"
                  label="Price"
                  value={this.props.price}
                  name="price"
                  onInputChange={this.handleInputChange}
                />
                 Total Quantity : {this.props.quantity}
                <RangeSlider min={1} max={10} step={1} value={this.props.quantity} onRangeChange={this.handleRangeChange}/>
              </Typography>
            </CardContent>
            <CardActions>
              <Button
                variant="outlined"
                color="primary"
                size="medium"
                onClick={()=>this.props.editProduct({id:this.props.match.params.id,name:this.props.productName,price:this.props.price,quantity:this.props.quantity})}
              >
                Add
              </Button>
            </CardActions>
          </Card>
        </>
      );
    }
}

const mapStateToProps = (state)  =>({
    variant:state.registrationReducer.registration.variant,
    message:state.registrationReducer.registration.validationMessage,
    isOpen:state.registrationReducer.registration.isOpen,
    productName:state.productReducer.productName,
    price:state.productReducer.price,
    quantity:state.productReducer.quantity,
})

const mapDispatchToProps = (dispatch) => ({
    closeMessageBox : () => dispatch(closeMessageBoxReg()),
    editProduct: (data) => dispatch(editProductInfo(data)),
    getProductDetail:(data) => dispatch(getSingalProductData(data)),
    productInputChange:(name,value) => dispatch(productInputChange({name:name,value:value}))
})
 
export default connect(mapStateToProps,mapDispatchToProps)(withStyles(styles)(EditProduct));