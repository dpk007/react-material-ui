import React ,{ Component } from 'react';

class Form extends Component {

    constructor(props) {
        super(props);
        this.state = {flavour: 'coconut',name:'Dharam',info:'Enter your information.'};
    
        this.handleName = this.handleName.bind(this);
        this.handleFlavour = this.handleFlavour.bind(this);
        this.handleInfo = this.handleInfo.bind(this);

        this.handleSubmit = this.handleSubmit.bind(this);
      }

      //handle info
      handleInfo(event){
        this.setState({info: event.target.value});
      }
       
      //handle flavours
      handleFlavour(event){
        this.setState({flavour: event.target.value});
      }

      //handle name change
      handleName(event) {
        //   console.log(event.target.value);
        this.setState({name: event.target.value});
      }
    
      handleSubmit(event) {
          
        event.preventDefault();
      }

   render(){
       return (
        <form onSubmit={this.handleSubmit}>
        <label>
         Name:
         <input type="text" value={this.state.name} placeholder="Enter Name" onChange={this.handleName} />
       </label>
       <br/><br/>
       <label>
         Info:
         <textarea value={this.state.info} placeholder="Enter Info" onChange={this.handleInfo} />
       </label>
       <br/><br/>
         <label>
           Pick your favorite flavor:
           <select value={this.state.flavour} onChange={this.handleFlavour}>
             <option value="grapefruit">Grapefruit</option>
             <option value="lime">Lime</option>
             <option value="coconut">Coconut</option>
             <option value="mango">Mango</option>
           </select>
         </label>
         <br/><br/>
         <input type="submit" value="Submit" />
       </form>
       )
   }  

}

export default Form;