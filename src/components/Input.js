import React, { Component } from 'react';
import { withStyles} from '@material-ui/core/styles';
import purple from '@material-ui/core/colors/purple';
import TextField from '@material-ui/core/TextField';



const styles = theme => ({
    root: {
      display: 'flex',
      flexWrap: 'wrap',
    },
    margin: {
      margin: theme.spacing.unit,
    },
    cssLabel: {
      '&$cssFocused': {
        color: purple[500],
      },
    },
    cssFocused: {},
    cssUnderline: {
      '&:after': {
        borderBottomColor: purple[500],
      },
    },
    cssOutlinedInput: {
      '&$cssFocused $notchedOutline': {
        borderColor: purple[500],
      },
    },

  });
  
  


class Input extends Component {
    constructor(props) {
        super(props);
         this.handleChange = this.handleChange.bind(this);
    }

    handleChange(e) {
        
        this.props.onInputChange(e.target.name,e.target.value);
      }

    render() { 
        return ( 

            <div className={this.props.classes.root}>
               <TextField
                         className={this.props.classes.margin}
                           InputLabelProps={{
                         classes: {
                                root: this.props.classes.cssLabel,
                                focused: this.props.classes.cssFocused,
                                  },
                               }}
                        InputProps={{
                      classes: {
                              root: this.props.classes.cssOutlinedInput,
                               focused: this.props.classes.cssFocused,
                                notchedOutline: this.props.classes.notchedOutline,
                                        },
                                 }}
                                label={this.props.label}
                                   variant="outlined"
                                
                                   id={this.props.id}
                                   name={this.props.name}
                                   type={this.props.type}
                                   value={this.props.value}
                                   onChange={this.handleChange}
                                   />
            </div>

         );
    }
}
 
export default withStyles(styles)(Input);